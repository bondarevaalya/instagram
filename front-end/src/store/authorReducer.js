//types
const GET_AUTHOR = "author/GET_AUTHOR"
const GET_AUTHOR_FOLLOWING = "author/GET_AUTHOR_FOLLOWING"
const GET_AUTHOR_NOT_FOLLOWING = "author/GET_AUTHOR_NOT_FOLLOWING"
const EDIT_AUTHOR = "author/EDIT_AUTHOR"
const EDIT_AUTHOR_FOLLOWING = "author/EDIT_AUTHOR"
const EDIT_AUTHOR_NOT_FOLLOWING = "author/EDIT_AUTHOR"
const GET_ONE_AUTHOR = "author/GET_ONE_AUTHOR"

//actions
const getOneAuthor = (author) => ({
    type: GET_ONE_AUTHOR,
    payload: author
})

const getFollowingAuthor = (authors) => ({
    type: GET_AUTHOR_FOLLOWING,
    payload: authors
})
const getAuthor = (authors) => ({
    type: GET_AUTHOR,
    payload: authors
})
const getNotFollowingAuthor = (authors) => ({
    type: GET_AUTHOR_NOT_FOLLOWING,
    payload: authors
})

const updateAuthor = (author) => ({
    type: EDIT_AUTHOR,
    payload: author
})

const updateAuthorFollowing = (author) => ({
    type: EDIT_AUTHOR_FOLLOWING,
    payload: author
})

const updateAuthorNotFollowing = (author) => ({
    type: EDIT_AUTHOR_NOT_FOLLOWING,
    payload: author
})

//fetch
export const getAllAuthors = () => async (dispatch) => {
    const res = await fetch("/api/authors/")
    if (res.ok) {
        const data = await res.json()
        dispatch(getAuthor(data))
        return null
    }
}
export const getAllFollowingAuthors = (query) => async (dispatch) => {
    const res = await fetch(`/api/authors/?following=${query}`)
    if (res.ok) {
        const data = await res.json()
        if (query) {
            dispatch(getFollowingAuthor(data))
            return null
        }
        if (!query) {
            dispatch(getNotFollowingAuthor(data))
            return null
        }
    }
}

export const editAuthor = (author, id) => async (dispatch) => {
    console.log(author);
    const res = await fetch(`/api/authors/${id}`, {
        method: "PUT",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(author)
    })
    if (res.ok) {
        const data = await res.json()
        dispatch(getOneAuthor(data))
        dispatch(updateAuthor(data))
        dispatch(updateAuthorFollowing(data))
        dispatch(updateAuthorNotFollowing(data))
    }
}

export const getAuthorById = (id) => async (dispatch) => {
    const res = await fetch(`/api/authors/${id}`)
    if (res.ok) {
        const data = await res.json()
        dispatch(getOneAuthor(data))
    }
}

//initialState
const initialState = {
    authors: {},
    following: {},
    notFollowing: {},
    author: {}
}
//reducer
const authorReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_AUTHOR_FOLLOWING:
            const followingState = payload.reduce((acc, cur) => {
                acc[cur._id] = cur
                return acc
            }, {})
            return { ...state, following: followingState }
        case GET_AUTHOR_NOT_FOLLOWING:
            const notFollowingState = payload.reduce((acc, cur) => {
                acc[cur._id] = cur
                return acc
            }, {})
            return { ...state, notFollowing: notFollowingState }
        case GET_AUTHOR:
            const authorState = payload.reduce((acc, cur) => {
                acc[cur._id] = cur
                return acc
            }, {})
            return { ...state, authors: authorState }
        case EDIT_AUTHOR:
            console.log(1)
            const updatedState = { ...state }
            updatedState.authors[payload.id] = payload
            return updatedState
        case EDIT_AUTHOR_FOLLOWING:
            console.log(2)
            const updatedStateFollowing = { ...state }
            if(payload.id in updatedStateFollowing.following){
                delete updatedStateFollowing.following[payload.id]
                return updatedStateFollowing
            }
            updatedStateFollowing.following[payload.id] = payload
            return updatedStateFollowing
        case EDIT_AUTHOR_NOT_FOLLOWING:
            console.log(3)
            const updatedStateNotFollowing = { ...state }
            if(payload.id in updatedStateNotFollowing.notFollowing){
                delete updatedStateNotFollowing.notFollowing[payload.id]
                return updatedStateNotFollowing
            }
            updatedStateNotFollowing.notFollowing[payload.id] = payload
            return updatedStateNotFollowing
        case GET_ONE_AUTHOR:
            return {...state, author: payload}

        default: return state
    }
}

export default authorReducer