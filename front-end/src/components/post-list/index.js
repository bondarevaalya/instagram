import React from 'react'
import { useSelector } from 'react-redux'
import PostCard from "../post-card"

const PostList = () => {
  const posts = useSelector(state => Object.values(state.posts.posts))
  return (
    <div className="col-span-2">
      <ul className='flex flex-col gap-10'>
        {posts.map(item => (
          <PostCard key={item._id} id={item._id} />
        ))}
      </ul>
    </div>
  )
}

export default PostList 