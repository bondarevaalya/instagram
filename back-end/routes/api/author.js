const router = require('express').Router();
const bodyParser = require("body-parser")
const Author = require("../../model/Author.js")
const Post = require("../../model/Post.js")


router.get('/:id/posts', async (req, res) => {
    const author = await Author.findById(req.params.id)
    if (!author) {
        res.status(404).json({ "message": `author ${req.params.id} not found `})
    }
    const posts = await Post.find({ author: author.nickName })
    return res.status(200).json(posts)
})

router.get('/:id', async (req, res) => {
    const author = await Author.findById(req.params.id)
    if (!author) {
        res.status(404).json({ "message": `author ${req.params.id} not found` })
    }
    return res.status(200).json(author)
})

router.get('/', async (req, res) => {
    const author = await Author.find(req.query)
    return res.status(200).json(author)
})

router.put('/:id', bodyParser.json(), async (req, res) => {
    const author = await Author.findById(req.params.id)
    if (!author) {
        res.status(404).json({ "message": `author ${req.params.id} not found` })
    }
    const updated = {
        _id: author.id,
        nickName: author.nickName,
        following: !author.following,
        imageUrl: author.imageUrl
    }
    const updateAuthor = await Author.replaceOne({_id:author._id}, updated)
    const newAuthor = await Author.findById(req.params.id)
    console.log(newAuthor)
    return res.status(200).json(newAuthor)
})

router.delete('/:id', async (req, res) => {
    const author = await Author.deleteOne(req.params.id)
    return res.status(200).json({ "message": "succsessful delete" })
})
module.exports = router;