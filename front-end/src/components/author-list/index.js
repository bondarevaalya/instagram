import React from 'react'
import { useSelector } from 'react-redux'
import AuthorCard from '../author-card'

export const AuthorList = () => {
    const following = useSelector(state => Object.values(state.authors?.following)) || []
    const notFollowing = useSelector(state => Object.values(state.authors?.notFollowing)) || []

    return (
        <div>
                        <h4 className='mb-[15px]'>following</h4>
            <div className='flex flex-col gap-5 h-[250px] overflow-scroll'>
            <ul className="flex flex-col gap-4 ">
                {following.map(item => (
                    <AuthorCard key={item._id} status="following" id={item._id} />
                ))}
            </ul>
            </div>
            <h4 className='mb-[15px]'>not following</h4>
            <div className='flex flex-col mt-6 gap-5'>
            <ul  className="flex flex-col gap-4">
                {notFollowing.map(item => (
                    <AuthorCard key={item._id} status="notFollowing" id={item._id} />
                ))}
            </ul>
            </div>
        </div>
    )
}