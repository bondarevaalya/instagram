//types
const GET_POSTS = "posts/GET_POSTS"
const UPDATE_POST = "posts/UPDATE_POST"
const GET_POSTS_BY_AUTOR_ID = "posts/GET_POSTS_BY_AUTOR_ID"

//actions
const getPosts = (posts) => ({
    type: GET_POSTS,
    payload: posts
})

const updatePost = (post) => ({
    type: UPDATE_POST,
    payload: post
})

const getPostsByAuthor = (posts) => ({
    type: GET_POSTS_BY_AUTOR_ID,
    payload: posts
})

export const getAllPosts = () => async (dispatch) => {
    const res = await fetch("/api/posts")
    if (res.ok) {
        const data = await res.json()
        dispatch(getPosts(data))
        return null
    }
}

export const editPost = (post, id) => async (dispatch) => {
    const res = await fetch(`/api/posts/${id}`, {
        method: "PUT",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(post)
    })
    if (res.ok) {
        const data = await res.json()
        dispatch(updatePost(data))
        return null
    }
}

export const getPostByAuthorId = (id) => async (dispatch) => {
    const res = await fetch(`/api/authors/${id}/posts`)
    if (res.ok) {
        const data = await res.json()
        console.log(data)
        dispatch(getPostsByAuthor(data)) 
        return null
    }
}

const initialState = {
    posts: {},
    postsByAuthor: {}
}

const postReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_POSTS:
            const newState = payload.reduce((acc, cur) => {
                acc[cur._id] = cur
                return acc
            }, {})
            return { ...state, posts: newState }
        case UPDATE_POST:
            return state
        case GET_POSTS_BY_AUTOR_ID:
            const postsAuthor = payload.reduce((acc, cur) => {
                acc[cur._id] = cur
                return acc
            }, {})
            return { ...state, postsByAuthor: postsAuthor }

        default: return state
    }
}
export default postReducer