const {Schema, model} = require("mongoose")

const schema = new Schema({
    title: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    imageUrl: {
        type: String,
    },
    author: {
        type: String,
        required: true
    },
    countOfLikes: {
        type: Number,
    },
    like: {
        type: String,
    }
})

module.exports = model("Post", schema)