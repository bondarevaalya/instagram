import React from "react";
import { Route, createBrowserRouter, createRoutesFromElements, RouterProvider } from "react-router-dom";

import { HomePage } from "./pages/home";
import {NotFoundPage} from "./pages/pageIsNotFound"
import { AuthorPage } from "./pages/AuthorPage";

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route>
            <Route path="/" element={<HomePage/>}/>
            <Route path="/authors/:id" element={<AuthorPage/>}/>
            <Route path="*" element={<NotFoundPage/>}/>
        </Route>
    )
);

export function App() {
    return (
        <RouterProvider router={router}/>
    );
}