import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getAllAuthors, getAllFollowingAuthors } from "../../store/authorReducer";
import { getAllComments } from "../../store/commentsReducer";
import { getAllPosts } from "../../store/postReducer";


import { AuthorList } from "../../components/author-list"
import PostList from "../../components/post-list"

export function HomePage() {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getAllAuthors())
        dispatch(getAllComments())
        dispatch(getAllPosts())
        dispatch(getAllFollowingAuthors(true))
        dispatch(getAllFollowingAuthors(false))
    }, [dispatch])
    return (
        <>
            <header className="fixed top-0 left-0 right-0 bg-red-400 h-20">
                <div className="container mx-auto max-w-[1200px]"><h1 className="text-3xl text-white uppercase mt-2">instagram</h1></div>
            </header>
            <main className="p-[100px]">
                <div className="container mx-auto grid gap-20 grid-cols-3 max-w-[1200px]">
                    <PostList />
                    <AuthorList />
                </div>
            </main>
        </>
    )
    }