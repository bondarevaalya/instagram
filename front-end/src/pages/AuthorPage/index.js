import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux"
import { getAuthorById } from "../../store/authorReducer";
import Button from "../../components/button/index"
import { getPostByAuthorId } from "../../store/postReducer";
import Modal from "../../components/modal"
import { toggleModal } from "../../store/modalReducer";
import { FaRegComment } from 'react-icons/fa';
import { AiOutlineHeart } from 'react-icons/ai';
import { getCommentsForPost } from "../../store/commentsReducer.js"
import { editAuthor } from "../../store/authorReducer";

export function AuthorPage() {
    const { id } = useParams()
    const author = useSelector(state => state.authors?.author)
    const posts = useSelector(state => Object.values(state.posts?.postsByAuthor))
    const isModalOpen = useSelector(state => state.modal.isModalOpen)
    const comments = useSelector(state => Object.values(state.comments.commentsForPost))
    // const [isLikeOpen ,setLikesOpen] = useState(false)
    const { nickName, imageUrl, following } = author
    const [isHover, setHover] = useState(null)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getAuthorById(id))
        dispatch(getPostByAuthorId(id))
    }, [dispatch, id])
    
    const handleClick = () => {
        dispatch(editAuthor({...author, following: !author.following}, id))
    }
    const handleImgClick = (data) => {
        dispatch(toggleModal(data))
    }

    return (
        <main className="mt-4">
            <section>
                <div className="container flex justify-around mx-auto max-w-[1200px]">
                    <img src={imageUrl} alt={nickName} className="rounded-[50%] w-[100px] h-[100px]" />
                    <div className="flex flex-col gap-4">
                        <h4>{nickName}</h4>
                        <Button className="bg-red-400 max-w-[100px] text-white p-2 rounded-md" text={following ? "unfollow" : "follow"} onClick={handleClick} />
                    </div>
                </div>
            </section>
            <section className="mt-10">
                <div className="container mx-auto max-w-[1200px]">
                    <ul className="grid grid-cols-5 gap-4">
                        {posts.map(item => (
                            <li
                                className="relative"
                                onMouseLeave={() => {
                                    setHover(null)
                                }}
                                onMouseEnter={() => {
                                    dispatch(getCommentsForPost(item._id))
                                    setHover(item._id)
                                }}
                                onClick={() => handleImgClick(item)}
                                key={item._id}>
                                <img className="h-[300px] w-[100%]" src={item.imageUrl} alt={item.title} />
                                {(item._id===isHover) && <div className=" absolute top-0 bg-black/50 opacity-[50px] h-[300px] w-[100%] flex justify-center items-center">
                                    <div>
                                        <p className="text-white"><FaRegComment />{comments.length}</p>
                                        <p className="text-white"><AiOutlineHeart />{item.countOfLikes}</p>
                                    </div>
                                </div>}
                            </li>))}
                    </ul>
                </div>
            </section>
            {isModalOpen && <Modal />}
        </main>)
}