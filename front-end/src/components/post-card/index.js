import React, { useEffect, useState } from 'react'
import Button from '../button'
import { useSelector, useDispatch } from 'react-redux'
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';
import { editPost } from '../../store/postReducer';

const PostCard = ({ id }) => {
  const post = useSelector(state => state.posts[id])
  const comments = useSelector(state => Object.values(state.comments)) || []
  const filteredComments = comments.filter(item => item.post === id)
  const [isCommentShow, setCommentShow] = useState(false)


  const firstComment = filteredComments[0]
  const restComments = [...filteredComments].toSpliced(1)
const dispatch = useDispatch()

  const handleClick = () => {
    setCommentShow(!isCommentShow)
  }

  const handleDoubleClick = (e) => {
    e.preventDefault()
    dispatch(editPost({...post, like: !post.like, countOfLikes:post.countOfLikes++, author:"64bcd9ec53ce81aac61f2d86"}, id))
  }

  return (
    <li className='border-2 border-red-400 border-solid p-5 flex flex-col gap-4'>
      <h2>{post?.title}</h2>
      <img className="w-[100%]" src={post?.imageUrl} alt={post?.title} onDoubleClick={handleDoubleClick} />
      <div className='flex gap-4'>
        {
          !post?.like && <AiOutlineHeart className='text-red-400' />
        }
        {
          post?.like && <AiFillHeart className='text-red-400' />
        }
        <p>likes: {post?.countOfLikes}</p>
      </div>
      <p>{post?.text}</p>
      <ul className='flex flex-col gap-4'>{!!filteredComments.length && <li>{firstComment.comment}</li>}
        {isCommentShow && restComments.toSpliced(1).map((item) => (

          <li key={item._id}>{item.comment}</li>
        ))}
      </ul>
      {filteredComments.length > 1 && <Button className="bg-red-400 max-w-[100px] text-white p-2 rounded-md" text={isCommentShow ? "Hide" : "Show more"} onClick={handleClick} />}
    </li>
  )
}

export default PostCard