import React, {useState} from 'react'
import { useDispatch, useSelector} from 'react-redux'
import Button from "../button"
import {editAuthor} from "../../store/authorReducer.js"
import {NavLink} from "react-router-dom"

const AuthorCard = ({ id, status }) => {
  const {nickName, imageUrl, following} = useSelector(state => state.authors?.[status][id]) || {}
  const [isNotFollowing, setIsNotFollowing] = useState(false)
  const dispatch = useDispatch()
  const handleClick = () => {
    try{
      dispatch(editAuthor({nickName, imageUrl, following: !following, id}, id))
      setIsNotFollowing(!isNotFollowing)
    }
    catch(e){
      console.error(e)
    }
  }
  return (
    <li className="flex gap-4">
      <NavLink to={`/authors/${id}`} className="flex flex-col gap-2 justify-center max-w-[100px] w-[100%]">
      <img src={imageUrl} alt={nickName} className='max-w-[70px] w-[100%] h-[70px] rounded-[50%] '/>
      <h2>{nickName}</h2>
      </NavLink>
      <div className='flex flex-col p-5 gap-4'>
      {status === "notFollowing" && <Button text={isNotFollowing?"not follow":"follow"} onClick={handleClick} className="text-red-400"/>}
      </div>
    </li>
  )
}

export default AuthorCard