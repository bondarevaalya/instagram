import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import commentsReducer from "./commentsReducer";
import authorReducer from "./authorReducer";
import postReducer from "./postReducer";
import modalReducer from "./modalReducer";

const rootReducer = combineReducers({
comments: commentsReducer,
authors: authorReducer,
posts: postReducer,
modal: modalReducer
});


let enhancer;

if (process.env.NODE_ENV === "production") {
  enhancer = applyMiddleware(thunk);
} else {
  const logger = require("redux-logger").default;
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  enhancer = composeEnhancers(applyMiddleware(thunk, logger));
}

const configureStore = (preloadedState) => {
  return createStore(rootReducer, preloadedState, enhancer);
};

export default configureStore;