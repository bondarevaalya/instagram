const router = require('express').Router();
const Comment = require("../../model/Comment.js")
const Post = require("../../model/Post.js")

router.get('/:id/comments', async (req, res) => { 
    const post = await Post.findById(req.params.id) 
        if(!post){
        res.status(404).json({"message": `post ${req.params.id} not found`})
    }
    const comments = await Comment.find({post: post.title}) 
    return res.status(200).json(comments)
})

// router.get('/:id', async (req, res) => { 
//     const post = await Post.findById(req.params.id) 
//     return res.status(200).json(post)
// })

router.get('/', async (req, res) => {  
    const posts = await Post.find() 
    return res.status(200).json(posts) 
    
}) 

router.put('/:id', async (req, res) => {
    const post = await Post.findById(req.params.id)
    if(!post){
        res.status(404).json({"message": `post ${req.params.id} not found`})
    }
    const updatePost = await Post.findByIdAndUpdate(req.params.id, req.body, { new: true });
        await updatePost.save();
    return res.status(200).json(updatePost)
})

module.exports = router;