const {Schema, model} = require("mongoose")

const schema = new Schema({
    comment: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true
    },
    post: {
        type: String,
        required: true
    },
})

module.exports = model("Comment", schema)