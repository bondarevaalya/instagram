const GET_COMMENTS = "comments/GET_COMMENTS"
const CREATE_COMMENT = "comments/CREATE_COMMENT"
const COMMENTS_FOR_POST = "comments/COMMENTS_FOR_POST"

const getComments = (comments) => ({
    type: GET_COMMENTS,
    payload: comments
})

const commentsForPost = (comments) => ({
    type: COMMENTS_FOR_POST,
    payload: comments
})

const createComment = (comment) => ({
    type: CREATE_COMMENT,
    payload: comment
})

export const getAllComments = () => async (dispatch) => {
    const res = await fetch("/api/comments")
    if (res.ok) {
        const data = await res.json()
        dispatch(getComments(data))
        return null
    }
}

export const getCommentsForPost = (id) => async (dispatch) => {
    const res = await fetch(`/api/posts/${id}/comments`)
    if (res.ok) {
        const data = await res.json()
        dispatch(commentsForPost(data))
        return null
    }
}

export const addComment = (comment) => async (dispatch) => {
    const res = await fetch("/api/comments", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(comment)
    })
    if (res.ok) {
        const data = await res.json()
        dispatch(createComment(data))
        return null
    }
} 
const initialState = {
    comments: {},
    commentsForPost: {}
}
const commentsReducer = (state = initialState, {type, payload})=>{
    switch (type){

        case GET_COMMENTS: 
        const newState = payload.reduce((acc, cur) => {
            acc[cur._id] = cur
            return acc
        }, {})
        return {...state, comments: newState}

        case CREATE_COMMENT:
            const newComment = {}
            newComment[payload.id] = payload

        return {...state, newComment}

        case COMMENTS_FOR_POST:
            const commentsForPost = payload.reduce((acc, cur) => {
                acc[cur._id] = cur
                return acc
            }, {})
            return {...state, commentsForPost: commentsForPost}
    
        default: return state
    }
} 
export default commentsReducer