import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { toggleModal } from "../../store/modalReducer"
import Button from "../../components/button/index"
import { getCommentsForPost } from "../../store/commentsReducer.js"

const Modal = () => {
    const data = useSelector(state => state.modal.data)
    console.log(data)
    const comment = useSelector(state => Object.values(state.comments.commentsForPost))
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getCommentsForPost(data._id))
    }, [dispatch, data._id])
    return (
        <div onClick={(e) => {
            dispatch(toggleModal({}))
        }} className="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full">
            <div className="relative w-full max-w-2xl max-h-full" onClick={e => e.stopPropagation()}>
                <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <div className="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                        <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
                            {data.title}
                        </h3>
                        <button type="button" onClick={() => dispatch(toggleModal({}))} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="defaultModal">
                            <svg className="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                            </svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    <div className="p-6 space-y-6">
                        <img src={data.imageUrl} alt={data.title} />
                        <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                            {data.text}
                        </p> 
                        <ul>
                            {comment.map(item => (
                                <li key={item._id}>
                                    {item.comment}
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                        <Button onClick={() => dispatch(toggleModal({}))} type="button" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" text="close" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal