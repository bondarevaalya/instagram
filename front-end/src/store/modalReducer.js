const TOGGLE_MODAL = "moda/TOGGLE_MODAL"

export const toggleModal = (data) => ({
    type: TOGGLE_MODAL,
    payload: data
})

const initialState = {
    data: {},
    isModalOpen: false
}

const modalReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case TOGGLE_MODAL:
            return { ...state, data: payload,isModalOpen: !state.isModalOpen }
        default: return state
    }
}
export default modalReducer